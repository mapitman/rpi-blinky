var Gpio = require('onoff').Gpio;
var led = {};

led['red'] = new Gpio(17, 'out')
led['yellow'] = new Gpio(18, 'out');
led['green'] = new Gpio(27, 'out');

var currentColor = 'green';
var direction = down;
var stopSweep = false;
process.on('SIGINT', exit);

exports.release = function() {
  exit();
}

exports.sweep = function() {
  stopSweep = false;
  sweep();
}

exports.allOff = function() {
  allOff();
}

exports.turnOn = function(color) {
  turnOn(color);
}

exports.turnOff = function(color) {
  turnOff(color);
}

exports.stop = function() {
  stopSweep = true;
}

function sweep() {
  direction();
  turnOn(currentColor);
  if (!stopSweep) {
    setTimeout(sweep, 100);
  } else {

  }
}

function stop() {
  stopSweep = true;
}

function down() {
  turnOff(currentColor);

  switch (currentColor) {
    case 'red':
      currentColor = 'yellow';
      break;
    case 'yellow':
      currentColor = 'green';
      break;
    case 'green':
      direction = up;
      break;
  }
}

function up() {
  turnOff(currentColor);

  switch (currentColor) {
    case 'red':
      direction = down;
      break;
    case 'yellow':
      currentColor = 'red';
      break;
    case 'green':
      currentColor = 'yellow';
      break;
  }
}

function turnOn(color) {
  led[color].writeSync(1);
}

function turnOff(color) {
  led[color].writeSync(0);
}

function allOff() {
  turnOff('red');
  turnOff('green');
  turnOff('yellow');
}

function exit() {
  console.log('Exiting...');
  led['red'].unexport();
  led['yellow'].unexport();
  led['green'].unexport();
  process.exit();
}
