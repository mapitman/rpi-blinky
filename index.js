var leds = require('./leds.js');

leds.sweep();

setTimeout(function() {
  leds.stop();
  leds.release();
}, 5000);
